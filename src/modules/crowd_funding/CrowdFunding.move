address 0x34887221288236afCA7bD93cDFb3B4f7 {
module CrowdFunding {
    //use 0x1::Option::{Self, Option};
    use 0x1::Token::{Self, Token};
    use 0x1::STC::STC;
    use 0x1::Signer;
    //use 0x1::Account;
    use 0x1::Vector;

    struct Contribution has store {
        account_address: address,
        value: u128,
    }

    struct CrowdFunding has key,store {
        total: u128,
        current: u128,
        contributions: vector<Token<STC>>,
        history: vector<Contribution>,
    }

    public fun init(account: &signer) {
        let addr = Signer::address_of(account);
        if (!exists<CrowdFunding>(addr)){
            move_to(account, CrowdFunding {
                total: 0,
                current: 0,
                contributions: Vector::empty<Token<STC>>(),
                history: Vector::empty<Contribution>(),
            });
        };
    }

    fun can_contribution(contribution_address: address) : bool {
        exists<CrowdFunding>(contribution_address)
    }

    public fun contribution(account: &signer, contribution_address: address, contribution: Token<STC>) acquires CrowdFunding {
        assert(Self::can_contribution(contribution_address), 40000);
        let stc = Token::value<STC>(&contribution);
        assert(stc > 0, 40001);
        let my_address = Signer::address_of(account);
        let my_contribution = Contribution{
            account_address: my_address,
            value: stc,
        };
        let crowd_funding = borrow_global_mut<CrowdFunding>(contribution_address);
        Vector::push_back(&mut crowd_funding.history, my_contribution);
        Vector::push_back(&mut crowd_funding.contributions, contribution);
        crowd_funding.total = crowd_funding.total + stc;
        crowd_funding.current = crowd_funding.current + stc;
    }

    public fun apply_contribution(account: &signer, amount:u128) : Token<STC> acquires CrowdFunding {
        let my_address = Signer::address_of(account);
        assert(Self::can_contribution(my_address), 40002);
        assert(amount > 0, 40003);
        let crowd_funding = borrow_global_mut<CrowdFunding>(my_address);
        assert(crowd_funding.current >= amount, 40004);
        let len = Vector::length(&crowd_funding.contributions);
        let stc = Token::zero<STC>();
        while (len > 0) {
            let back = Vector::pop_back(&mut crowd_funding.contributions);
            stc = Token::join<STC>(back, stc);
            len = len - 1;
            let tmp = Token::value<STC>(&stc);
            if (tmp >= amount) {
                break
            };
        };

        stc
    }
}
}
